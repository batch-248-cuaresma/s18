//Functions 
 //Function aremostly created for complicated tasks to run several line of code in succession.
  //Function are also used to prevent repeating line/blocks of codes that perform the same task/functionality.

  let nickName = "alpha";

  function printInput() {
    // let nickName = prompt("Enterr your nickname: ")
    console.log("hi!, " + nickName)

  }

//   printInput()
 //Parameter
function printName(name){
    console.log("My nickName is " + name);
}

printName("alpha");
printName("joy");

//you can directly pass  data into the function

//[Section] Parameter and Arguments

        //Parameter
            // "name" is called a paremeter.
            // A "parameter" acts as a named variable that only exists inside a function.
        //Arguments 
            // "alpha" the information provided directly into the function is call an argument
            // Values passed when  invoking the function. ex. functionName(argument);

function checkDivisibilityBy8(num){
    let remainder = num % 8;
    console.log("The remainder of " +num+ " divide by 8 is " +remainder);
    let isDivisibleBy8 = remainder ===0;
    console.log("is " +num+ " divisible by 8?");
    console.log(isDivisibleBy8);
}

checkDivisibilityBy8(64);
checkDivisibilityBy8(25);


function checkDivisibilityBy4(num){
    let remainder = num % 4;
    console.log("The remainder of " +num+ " divide by 4 is " +remainder);
    let isDivisibleBy8 = remainder ===0;
    console.log("is " +num+ " divisible by 4?");
    console.log(isDivisibleBy8);
}

checkDivisibilityBy4(56)
checkDivisibilityBy4(95)

//[SECTION] Function as Argument
    //Function parameter can also accept other function as arguments.
    // Some complex functions use other function as arguments to perform more complicated result.

function argumentFunction(){
    console.log("This function was passed as an argument before the message was printend.")
}

function invokeFunction(argumentFunction){
    argumentFunction();
}
//fuction used without a parenthesis is nomally associated with using a Function as an ARGUMENT to another fucntion
invokeFunction(argumentFunction);

//this for finding information about a function in the console using console.log()
console.log(argumentFunction);

//[Section] using mulyiple parameters
    //Mulitiple "arguments" will corresponds to the number of parameters s

    function createFullName(firstName, middleName, LastName, character) {
        console.log(firstName + ' ' + middleName + ' ' + LastName + ' is the ' + character + '.');
    }

    createFullName("alpha", "joy","Cuaresma", "boss");
    createFullName("alpha", "joy","Cuaresma");
    createFullName("alpha", "joy","Cuaresma", "boss", "dito");


//use variable as arguments

let firstName = "Monkey";
let middleName= "D.";
let LastName =  "Luffy";
let character = "protagonist";

createFullName(firstName, middleName, LastName, character);

  function printFullName(middleName,fullName,LastName) {
    console.log(firstName + ' ' + middleName + ' ' + LastName);
  }


  printFullName("alpha", "joy","cuaresma")

  function createFriends(friend1,friend2,friend3) {
    console.log("My three friends are: " + friend1 + ' ' + friend2 + ' ' + friend3 );
}

createFriends("Jessa","Jennifer","Jocelyn");


function returnFullName(firstName,middleName,LastName){
    return firstName + " " + middleName + " " + LastName;
    console.log("this will not be printed");
}

let completeName = returnFullName("Karen", "L.", "Davila");

console.log(completeName + "is my friend");
console.log(completeName);

console.log(returnFullName(firstName,middleName,LastName));

function returnAddress(city,country){
    let fullAdress = city + "," + country;
    return fullAdress
}
let myAddress = returnAddress("tabuk", "agbannawag");
console.log(myAddress);

function printPlayerInfo(username, level, job){
    console.log("username: " + username);
    console.log("level: " + level);
    console.log("job: " + job);
}

let user1 = printPlayerInfo("alpha", 90, "paladin");
console.log(user1)

function promptName(name){
    promptName("Pealse enter your name");

}

function multiplyBy(num1,num2){
    return num1 * num2
}

let product = multiplyBy(5,2)

console.log(product);

let name = prompt("Enter your Name");
function printName1(name){
    console.log("hi ", name);
}
printName1(name)